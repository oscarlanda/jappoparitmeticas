/*
-------------------------------------------------------------------------------------------------------
   Tecnologia: Java
   Aplicacion: AppOpAritmeticas
Desarrollador: Oscar Barrios Landa
        Fecha: 19/09/2019
        Lugar: Iguala de la Independencia, Guerrero. México.
  Descripción: Aplicación que pide al usuario que escriba dos números, obtiene los números del usuario
               e imprime la suma, producto, diferencia y cociente (división) de los números.
-------------------------------------------------------------------------------------------------------
*/
package appoparitmeticas;

import java.util.*;

public class AppOpAritmeticas {
    
    public static void main(String[] args) 
    {
       Scanner entrada = new Scanner(System.in);
      
       int npNumero = 0;
       int nsNumero = 0;
       int nResultado = 0;
       int nOpcion = 0;
       
       System.out.println("Aplicacion Java!!!");
       System.out.println("Operaciones Aritmeticas con dos numeros Enteros");
       System.out.println("===============================================");
       
       System.out.print("Ingrese el primer numero: ");       
       npNumero = entrada.nextInt();
       
       System.out.print("Ingrese el segundo numero: ");
       nsNumero = entrada.nextInt();
       
       System.out.println("1) Sumar");
       System.out.println("2) Restar");
       System.out.println("3) Multiplicar");
       System.out.println("4) Dividir");
       System.out.print("Digite una Opcion: ");
       nOpcion = entrada.nextInt();
       
       if(nOpcion == 1)
           nResultado = npNumero + nsNumero;
       
       if(nOpcion == 2)
           nResultado = npNumero - nsNumero;
       
       if(nOpcion == 3)
           nResultado = npNumero * nsNumero;
       
       if(nOpcion == 4)
           nResultado = npNumero / nsNumero;
       
       System.out.println(nResultado);       
    }
    
}
